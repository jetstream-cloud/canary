FROM python:latest
RUN pip install --no-cache-dir python-openstackclient ansible
RUN ansible-galaxy collection install openstack.cloud