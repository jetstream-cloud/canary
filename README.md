# canary

canary is a continuous integration pipeline that creates instances on [Jetstream2](https://docs.jetstream-cloud.org) and runs tests on them. The pipeline runs on a [recurring schedule](https://gitlab.com/jetstream-cloud/canary/-/pipeline_schedules) to detect problems with Jetstream2 and alert on failures.

https://gitlab.com/jetstream-cloud/project-mgt/-/issues/59 is the original issue that led to the canary pipeline.

## How is this different from the image build pipeline?

The [image build pipeline](https://gitlab.com/jetstream-cloud/image-build-pipeline/) customizes and publishes operating system images for Jetstream2, while the canary pipeline's purpose is to _test whether the cloud works_ and _alert us if it fails_.

Aside from different purposes, the image pipeline is quite complex and long-running: it passes many GB of data, runs a lot of Ansible code, and installs a lot of packages. By comparison, the canary pipeline is small, fast, and laser-focused.

While the image build pipeline uses a custom CI runner instance on Jetstream2 itself, the canary pipeline uses gitlab.com's hosted runner environment. The canary pipeline uses the Docker executor with a custom Docker image (see `Dockerfile` in this repo).

## How is this similar to the image build pipeline?

Both pipelines spawn several child pipelines that run in parallel, either to build images for several operating systems at a time, or to test several instances / flavors at a time.

## Infrastructure and artifacts

Canary automatically creates instances in the `canary` project on Jetstream2, but it has no other persistent infrastructure (e.g. service instances) on the cloud itself.

The pipeline automatically cleans up old and broken instances, similar to the image build pipeline -- see `old_resource_cleanup` job.

## How to enable and disable canary pipeline.

We might want to temporarily disable the pipeline, e.g. during maintenance or outages, and re-enable it when operations resume.

- Log into GitLab with a user who is a member of the canary project (i.e. a member of `jetstream-cloud` namespace).
- Browse to <https://gitlab.com/jetstream-cloud/canary/-/pipeline_schedules>.
- If the current pipeline schedule has an owner who is _not_ you, click the "Take ownership" button.

![screenshot of button to take ownership of pipeline](docs/assets/img/pipeline-take-ownership.jpg)

- Click the pencil button to edit.
- Check or uncheck the "Activated" button on the Edit Pipeline Schedule page.
- Click "Save changes".

## How to verify operation

To run the pipeline on-demand, browse to the [pipelines page](https://gitlab.com/jetstream-cloud/canary/-/pipelines/) and click "Run pipeline". Watch it go to completion.

To verify that the pipeline runs on a schedule, browse to the [pipelines page](https://gitlab.com/jetstream-cloud/canary/-/pipelines/). Look at the list of pipelines and verify that they are created according to the schedule defined in the GitLab project.